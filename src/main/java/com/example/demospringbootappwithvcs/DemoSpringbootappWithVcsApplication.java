package com.example.demospringbootappwithvcs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringbootappWithVcsApplication {

    // I added this comment.
    public static void main(String[] args) {
        SpringApplication.run(DemoSpringbootappWithVcsApplication.class, args);
    }

}
